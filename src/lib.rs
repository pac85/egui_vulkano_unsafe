//! [egui](https://docs.rs/egui) rendering backend for [Vulkano](https://docs.rs/vulkano).

use std::collections::HashMap;
use std::iter::ExactSizeIterator;
use std::slice::SliceIndex;
use std::sync::Arc;

use bytemuck::{Pod, Zeroable};
use egui::util::cache::CacheTrait;
use egui::{Color32, Context, Rect, TextureId};
use epaint::{ClippedPrimitive, ClippedShape, ImageDelta, Primitive};
use vulkano::buffer::BufferAccess;
use vulkano::buffer::{BufferSlice, BufferUsage, CpuAccessibleBuffer};
use vulkano::command_buffer::SubpassContents::Inline;
use vulkano::command_buffer::{
    sys::*, BufferImageCopy, CommandBufferUsage, CopyBufferToImageInfo, RenderPassError,
};
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, AutoCommandBufferBuilderContextError, DrawIndexedError,
    PrimaryAutoCommandBuffer, PrimaryCommandBuffer,
};
use vulkano::descriptor_set::{
    DescriptorSet, DescriptorSetCreationError, PersistentDescriptorSet, WriteDescriptorSet,
};
use vulkano::device::{Device, Queue};
use vulkano::format::Format;
use vulkano::image::{
    ImageAccess, ImageCreationError, ImageDimensions, ImageSubresourceLayers, ImmutableImage,
    MipmapsCount, StorageImage,
};
use vulkano::pipeline::graphics::{
    color_blend::{AttachmentBlend, BlendFactor},
    input_assembly::IndexType,
    viewport::Scissor,
    GraphicsPipeline, GraphicsPipelineCreationError,
};
use vulkano::pipeline::{Pipeline, PipelineBindPoint};
use vulkano::sampler::{
    Filter, Sampler, SamplerAddressMode, SamplerCreateInfo, SamplerCreationError, SamplerMipmapMode,
};
use vulkano::shader::ShaderStages;
use vulkano::sync::{FlushError, GpuFuture};

mod shaders;

#[repr(C)]
#[derive(Default, Debug, Clone, Copy, Zeroable, Pod)]
pub struct Vertex {
    pub pos: [f32; 2],
    pub uv: [f32; 2],
    pub color: [f32; 4],
}

impl From<&epaint::Vertex> for Vertex {
    fn from(v: &epaint::Vertex) -> Self {
        let convert = {
            |c: Color32| {
                [
                    c.r() as f32 / 255.0,
                    c.g() as f32 / 255.0,
                    c.b() as f32 / 255.0,
                    c.a() as f32 / 255.0,
                ]
            }
        };

        Self {
            pos: [v.pos.x, v.pos.y],
            uv: [v.uv.x, v.uv.y],
            color: convert(v.color),
        }
    }
}

vulkano::impl_vertex!(Vertex, pos, uv, color);

use thiserror::Error;
use vulkano::command_buffer::pool::CommandPoolBuilderAlloc;
use vulkano::image::view::{ImageView, ImageViewCreationError};
use vulkano::memory::DeviceMemoryAllocationError;
use vulkano::render_pass::Subpass;

#[derive(Error, Debug)]
pub enum PainterCreationError {
    #[error(transparent)]
    CreatePipelineFailed(#[from] GraphicsPipelineCreationError),
    #[error(transparent)]
    CreateSamplerFailed(#[from] SamplerCreationError),
}

#[derive(Error, Debug)]
pub enum UpdateSetError {
    #[error(transparent)]
    CreateTextureFailed(#[from] CreateTextureError),
    #[error(transparent)]
    CreateImageViewFailed(#[from] ImageViewCreationError),
    #[error(transparent)]
    IncorrectDefinition(#[from] DescriptorSetCreationError),
    //#[error(transparent)]
    //BuildFailed(#[from] PersistentDescriptorSetBuildError),
}

#[derive(Error, Debug)]
pub enum DrawError {
    #[error(transparent)]
    RenderPassError(#[from] RenderPassError),
    #[error(transparent)]
    UpdateSetFailed(#[from] UpdateSetError),
    #[error(transparent)]
    NextSubpassFailed(#[from] AutoCommandBufferBuilderContextError),
    #[error(transparent)]
    CreateBuffersFailed(#[from] DeviceMemoryAllocationError),
    #[error(transparent)]
    DrawIndexedFailed(#[from] DrawIndexedError),
}

pub type EguiPipeline = GraphicsPipeline;

pub struct UserTexture {
    set: Arc<PersistentDescriptorSet>,
    image: Arc<dyn ImageAccess>,
}

impl UserTexture {
    pub fn from_vulkan_image(
        painter: &Painter,
        image: Arc<dyn ImageAccess>,
    ) -> Result<Self, CreateTextureError> {
        let layout = &painter.pipeline.layout().set_layouts()[0];
        let set = {
            PersistentDescriptorSet::new(
                layout.clone(),
                [WriteDescriptorSet::image_view_sampler(
                    0,
                    ImageView::new_default(image.clone()).unwrap(),
                    painter.sampler.clone(),
                )],
            )?
        };

        Ok(Self { set, image })
    }
}

#[derive(Clone)]
pub struct ManagedTexture {
    //texture: Arc<ImmutableImage>,
    set: Arc<PersistentDescriptorSet>,
    image: Arc<StorageImage>,
}

impl ManagedTexture {
    fn image_from_delta(
        device: Arc<Device>,
        queue: Arc<Queue>,
        delta: &ImageDelta,
        image: Arc<StorageImage>,
    ) -> Result<(), CreateTextureError> {
        let dims = ImageDimensions::Dim2d {
            width: delta.image.width() as u32,
            height: delta.image.height() as u32,
            array_layers: 1,
        };
        //let format = image.format();

        let data: Vec<_> = match &delta.image {
            egui::ImageData::Color(image) => {
                assert_eq!(
                    image.width() * image.height(),
                    image.pixels.len(),
                    "Mismatch between texture size and texel count"
                );
                image.pixels.iter().map(|color| color.to_array()).collect()
            }
            egui::ImageData::Font(image) => {
                let gamma = 1.0;
                image
                    .srgba_pixels(gamma)
                    .map(|color| color.to_array())
                    .collect()
            }
        };

        /*let (texture, future) = ImmutableImage::from_iter(
            data,
            dims,
            MipmapsCount::One,
            format,
            queue.clone(),
        )?;*/
        let staging = CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage {
                transfer_src: true,
                ..BufferUsage::none()
            },
            true,
            data.into_iter(),
        )
        .unwrap();

        let (offset, tsize) = if let Some([x, y]) = delta.pos {
            (
                [x as u32, y as u32, 0],
                [dims.width() as u32, dims.height() as u32, 1],
            )
        } else {
            ([0, 0, 0], [dims.width() as u32, dims.height() as u32, 1])
        };
        let mut copy_cb = AutoCommandBufferBuilder::primary(
            device.clone(),
            queue.family().clone(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        let copy_buffer_to_image_info = CopyBufferToImageInfo {
            regions: vec![BufferImageCopy {
                image_offset: offset,
                image_extent: tsize,
                image_subresource: ImageSubresourceLayers {
                    aspects: image.format().aspects(),
                    mip_level: 0,
                    array_layers: 0..1,
                },
                ..Default::default()
            }]
            .into(),
            ..CopyBufferToImageInfo::buffer_image(staging, image.clone())
        };
        copy_cb
            .copy_buffer_to_image(copy_buffer_to_image_info)
            .unwrap();

        let copy_cb = copy_cb.build().unwrap();

        //TODO remove unwrap
        copy_cb.execute(queue.clone()).unwrap().flush().unwrap();
        //future.then_execute(queue.clone(), copy_cb).unwrap().flush()?;

        Ok(())
    }
    pub fn new(painter: &Painter, delta: &ImageDelta) -> Result<Self, CreateTextureError> {
        let dims = ImageDimensions::Dim2d {
            width: delta.image.width() as u32,
            height: delta.image.height() as u32,
            array_layers: 1,
        };
        let format = Format::R8G8B8A8_UNORM;

        let image = StorageImage::new(
            painter.device.clone(),
            dims,
            format,
            std::iter::once(painter.queue.family()),
        )
        .unwrap();

        Self::image_from_delta(
            painter.device.clone(),
            painter.queue.clone(),
            delta,
            image.clone(),
        )?;

        let layout = &painter.pipeline.layout().set_layouts()[0];
        let set = {
            PersistentDescriptorSet::new(
                layout.clone(),
                [WriteDescriptorSet::image_view_sampler(
                    0,
                    ImageView::new_default(image.clone()).unwrap(),
                    painter.sampler.clone(),
                )],
            )
            .unwrap()
        };

        Ok(Self { set, image })
    }

    pub fn update(&self, device: Arc<Device>, queue: Arc<Queue>, delta: &ImageDelta) {
        Self::image_from_delta(device, queue, delta, self.image.clone());
    }
}

pub enum BackendTexture {
    Managed(ManagedTexture),
    User(UserTexture),
}

impl BackendTexture {
    pub fn set(&self) -> Arc<PersistentDescriptorSet> {
        match self {
            BackendTexture::Managed(m) => m.set.clone(),
            BackendTexture::User(u) => u.set.clone(),
        }
    }

    pub fn unwrap_user(self) -> UserTexture {
        match self {
            BackendTexture::Managed(m) => panic!("Expected a user texture"),
            BackendTexture::User(u) => u,
        }
    }
}

/// Contains everything needed to render the gui.
pub struct Painter {
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,
    pub pipeline: Arc<EguiPipeline>,
    pub subpass: Subpass,
    pub sampler: Arc<Sampler>,
    pub set: Option<Arc<PersistentDescriptorSet>>,
    pub buffers: Vec<Arc<dyn BufferAccess>>,
    pub tetxures: HashMap<egui::TextureId, BackendTexture>,
    last_user_texture_id: u64,
}

impl Painter {
    /// Pass in your vulkano `Device`, `Queue` and the `Subpass`
    /// that you want to use to render the gui.
    pub fn new(
        device: Arc<Device>,
        queue: Arc<Queue>,
        subpass: Subpass,
    ) -> Result<Self, PainterCreationError> {
        let pipeline = create_pipeline(device.clone(), subpass.clone())?;
        let sampler = create_sampler(device.clone())?;
        Ok(Self {
            device,
            queue,
            pipeline,
            subpass,
            sampler,
            set: None,
            buffers: vec![],
            tetxures: HashMap::new(),
            last_user_texture_id: 0,
        })
    }

    fn update_set(&mut self, egui_ctx: &Context) -> Result<(), UpdateSetError> {
        /*let texture = egui_ctx.texture();
        if texture.version == self.texture_version {
            return Ok(());
        }
        self.texture_version = texture.version;

        let layout = &self.pipeline.layout().descriptor_set_layouts()[0];
        let image = create_font_texture(self.queue.clone(), texture)?;

        let set = {
            PersistentDescriptorSet::new(layout.clone(), [WriteDescriptorSet::image_view_sampler(0, ImageView::new(image)?, self.sampler.clone())])?
        };

        self.set = Some(set);*/
        Ok(())
    }

    /// Pass in the `ClippedShape`s that egui gives us to draw the gui.
    pub fn draw<P>(
        &mut self,
        builder: &mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer<P::Alloc>, P>,
        window_size_points: [f32; 2],
        egui_ctx: &Context,
        clipped_shapes: Vec<ClippedShape>,
        textures_delta: &egui::TexturesDelta,
    ) -> Result<(), DrawError>
    where
        P: CommandPoolBuilderAlloc,
    {
        self.update_set(egui_ctx)?;
        builder.next_subpass(Inline)?;
        let clipped_meshes: Vec<ClippedPrimitive> = egui_ctx.tessellate(clipped_shapes);
        let num_meshes = clipped_meshes.len();
        let mut verts = Vec::<Vertex>::with_capacity(num_meshes * 4);
        let mut indices = Vec::<u32>::with_capacity(num_meshes * 6);
        let mut clips = Vec::<Rect>::with_capacity(num_meshes);
        let mut offsets = Vec::<(usize, usize)>::with_capacity(num_meshes);

        for ClippedPrimitive {
            clip_rect: clip,
            primitive,
        } in clipped_meshes.iter()
        {
            let mesh = match primitive {
                Primitive::Mesh(mesh) => mesh,
                Primitive::Callback(_) => continue,
            };
            // There's an incredibly weird edge case where epaint
            // will give us meshes with no actual content in them.
            // In that case, we skip rendering the mesh.
            // This also fixes a crash within vulkano that occurs
            // if we try to initialize a buffer with a length of 0
            // and then later try to slice into it (vulkano forces
            // a minimum size of 1 for all buffers, breaking an
            // assertion for self.size() / mem::size_of::<T>()).
            if mesh.vertices.len() == 0 || mesh.indices.len() == 0 {
                continue;
            }

            offsets.push((verts.len(), indices.len()));

            for v in mesh.vertices.iter() {
                verts.push(v.into());
            }

            for i in mesh.indices.iter() {
                indices.push(*i);
            }

            clips.push(*clip);
        }
        offsets.push((verts.len(), indices.len()));

        // Small optimization: If there's nothing to render,
        // return here instead of taking time to create an
        // empty (1 byte) buffer.
        if clips.len() == 0 {
            return Ok(());
        }

        let (vertex_buf, index_buf) = self.create_buffers((verts, indices))?;
        for (idx, clip) in clips.iter().enumerate() {
            let mut scissors = Vec::with_capacity(1);
            let o = clip.min;
            let (w, h) = (clip.width() as u32, clip.height() as u32);
            scissors.push(Scissor {
                origin: [o.x as u32, o.y as u32],
                dimensions: [w, h],
            });

            let offset = offsets[idx];
            let end = offsets[idx + 1];

            //let vb_slice = vb.clone().slice(offset.0..end.0).unwrap(); does not work
            let vb_slice = BufferSlice::from_typed_buffer_access(vertex_buf.clone())
                .slice(offset.0 as u64..end.0 as u64)
                .unwrap();
            let ib_slice = BufferSlice::from_typed_buffer_access(index_buf.clone())
                .slice(offset.1 as u64..end.1 as u64)
                .unwrap();

            builder.bind_pipeline_graphics(self.pipeline.clone());
            builder.bind_vertex_buffers(0, vec![vb_slice]);
            builder.bind_index_buffer(ib_slice.clone());
            builder.bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline.layout().clone(),
                0,
                self.set.clone().unwrap(),
            );
            builder.push_constants(self.pipeline.layout().clone(), 0, window_size_points);
            builder.draw_indexed(ib_slice.size() as u32 / 4, 1, 0, 0, 0)?;
        }
        Ok(())
    }

    ///lol
    pub fn unsafe_draw(
        &mut self,
        builder: &mut UnsafeCommandBufferBuilder,
        window_size_pixels: [f32; 2],
        window_size_points: [f32; 2],
        egui_ctx: &Context,
        clipped_shapes: Vec<ClippedShape>,
        textures_delta: &egui::TexturesDelta,
    ) -> Result<(), DrawError> {
        for (id, delta) in &textures_delta.set {
            self.set_texture(*id, delta);
        }
        for id in &textures_delta.free {
            self.free_texture(*id);
        }

        let clipped_meshes: Vec<ClippedPrimitive> = egui_ctx.tessellate(clipped_shapes);
        let num_meshes = clipped_meshes.len();
        let mut verts = Vec::<Vertex>::with_capacity(num_meshes * 4);
        let mut indices = Vec::<u32>::with_capacity(num_meshes * 6);
        let mut clips = Vec::<(Rect, egui::TextureId)>::with_capacity(num_meshes);
        let mut offsets = Vec::<(usize, usize)>::with_capacity(num_meshes);

        for ClippedPrimitive {
            clip_rect: clip,
            primitive,
        } in clipped_meshes.iter()
        {
            let mesh = match primitive {
                Primitive::Mesh(mesh) => mesh,
                Primitive::Callback(_) => continue,
            };

            // There's an incredibly weird edge case where epaint
            // will give us meshes with no actual content in them.
            // In that case, we skip rendering the mesh.
            // This also fixes a crash within vulkano that occurs
            // if we try to initialize a buffer with a length of 0
            // and then later try to slice into it (vulkano forces
            // a minimum size of 1 for all buffers, breaking an
            // assertion for self.size() / mem::size_of::<T>()).
            if mesh.vertices.len() == 0 || mesh.indices.len() == 0 {
                continue;
            }

            offsets.push((verts.len(), indices.len()));

            for v in mesh.vertices.iter() {
                verts.push(v.into());
            }

            for i in mesh.indices.iter() {
                indices.push(*i);
            }

            clips.push((*clip, mesh.texture_id));
        }
        offsets.push((verts.len(), indices.len()));

        // Small optimization: If there's nothing to render,
        // return here instead of taking time to create an
        // empty (1 byte) buffer.
        if clips.len() == 0 {
            return Ok(());
        }

        let (vertex_buf, index_buf) = self.create_buffers((verts, indices))?;
        self.buffers.clear();
        self.buffers.push(vertex_buf.clone());
        self.buffers.push(index_buf.clone());
        for (idx, (clip, texture_id)) in clips.iter().enumerate() {
            let mut scissors = Vec::with_capacity(1);
            let mut o = clip.min;
            let scale = window_size_pixels[0] / window_size_points[0];
            o.x = (o.x * scale).clamp(0.0, window_size_pixels[0]);
            o.y = (o.y * scale).clamp(0.0, window_size_pixels[1]);
            let (mut w, mut h) = (
                (clip.width() * scale) as u32,
                (clip.height() * scale) as u32,
            );
            w = w.clamp(0, window_size_pixels[0] as u32);
            h = h.clamp(0, window_size_pixels[1] as u32);
            scissors.push(Scissor {
                origin: [o.x as u32, o.y as u32],
                dimensions: [w, h],
            });

            let offset = offsets[idx];
            let end = offsets[idx + 1];

            let set = self.get_set(*texture_id);

            //let vb_slice = vb.clone().slice(offset.0..end.0).unwrap(); does not work
            let vb_slice = BufferSlice::from_typed_buffer_access(vertex_buf.clone())
                .slice(offset.0 as u64..end.0 as u64)
                .unwrap();
            let ib_slice = BufferSlice::from_typed_buffer_access(index_buf.clone())
                .slice(offset.1 as u64..end.1 as u64)
                .unwrap();

            unsafe {
                builder.bind_pipeline_graphics(&self.pipeline);
                builder.bind_vertex_buffers(0, {
                    let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                    builder.add(&vb_slice);
                    builder
                });
                builder.bind_index_buffer(&ib_slice, IndexType::U32);
                builder.bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    &self.pipeline.layout(),
                    0,
                    [set.as_ref().unwrap().inner()].iter().cloned(),
                    [].iter().cloned(),
                );
                builder.push_constants(
                    &self.pipeline.layout(),
                    ShaderStages {
                        vertex: true,
                        fragment: true,
                        ..ShaderStages::none()
                    },
                    0,
                    std::mem::size_of::<[f32; 2]>() as u32,
                    &window_size_points,
                );
                /*builder.set_viewport(
                    0,
                    ds.viewports.as_ref().unwrap().iter().cloned(),
                );*/
                builder.set_scissor(0, scissors.iter().cloned());
                builder.draw_indexed(ib_slice.size() as u32 / 4, 1, 0, 0, 0);
            }
        }
        Ok(())
    }

    pub fn set_texture(&mut self, id: egui::TextureId, delta: &ImageDelta) {
        if let Some(BackendTexture::Managed(ref mut texture)) = self.tetxures.get_mut(&id) {
            texture.update(self.device.clone(), self.queue.clone(), delta);
        } else {
            let texture = ManagedTexture::new(&self, delta).unwrap();
            self.tetxures.insert(id, BackendTexture::Managed(texture));
        }
    }

    pub fn insert_vulkan_image(
        &mut self,
        image: Arc<dyn ImageAccess>,
    ) -> Result<TextureId, CreateTextureError> {
        let texture_id = TextureId::User(self.last_user_texture_id);
        self.tetxures.insert(
            texture_id,
            BackendTexture::User(UserTexture::from_vulkan_image(&self, image)?),
        );
        Ok(texture_id)
    }

    pub fn remove_user_texture(&mut self, texture: TextureId) -> Option<Arc<dyn ImageAccess>> {
        if let Some(BackendTexture::User(_)) = self.tetxures.get(&texture) {
        } else {
            return None;
        }
        self.tetxures
            .remove(&texture)
            .map(|t| t.unwrap_user().image)
    }

    pub fn free_texture(&mut self, id: egui::TextureId) {
        self.tetxures.remove(&id);
    }

    fn get_set(&self, id: egui::TextureId) -> Option<Arc<PersistentDescriptorSet>> {
        self.tetxures.get(&id).map(|t| t.set())
    }

    /// Create vulkano CpuAccessibleBuffer objects for the vertices and indices
    fn create_buffers(
        &self,
        triangles: (Vec<Vertex>, Vec<u32>),
    ) -> Result<
        (
            Arc<CpuAccessibleBuffer<[Vertex]>>,
            Arc<CpuAccessibleBuffer<[u32]>>,
        ),
        DeviceMemoryAllocationError,
    > {
        let vertex_buffer = CpuAccessibleBuffer::from_iter(
            self.device.clone(),
            BufferUsage::vertex_buffer(),
            false,
            triangles.0.iter().cloned(),
        )?;

        let index_buffer = CpuAccessibleBuffer::from_iter(
            self.device.clone(),
            BufferUsage::index_buffer(),
            false,
            triangles.1.iter().cloned(),
        )?;

        Ok((vertex_buffer, index_buffer))
    }
}

/// Create a graphics pipeline with the shaders and settings necessary to render egui output
fn create_pipeline(
    device: Arc<Device>,
    subpass: Subpass,
) -> Result<Arc<EguiPipeline>, GraphicsPipelineCreationError> {
    let vs = shaders::vs::load(device.clone()).unwrap();
    let fs = shaders::fs::load(device.clone()).unwrap();

    let mut blend = AttachmentBlend::alpha();
    blend.color_source = BlendFactor::One;

    let pipeline = GraphicsPipeline::start()
        .vertex_input_single_buffer::<Vertex>()
        .vertex_shader(vs.entry_point("main").unwrap(), ())
        .triangle_list()
        .viewports_scissors_dynamic(1)
        .fragment_shader(fs.entry_point("main").unwrap(), ())
        .cull_mode_disabled()
        .blend_collective(blend)
        .render_pass(subpass)
        .build(device.clone())?;
    Ok(pipeline)
}

/// Create a texture sampler for the egui font texture
fn create_sampler(device: Arc<Device>) -> Result<Arc<Sampler>, SamplerCreationError> {
    Sampler::new(
        device.clone(),
        SamplerCreateInfo {
            address_mode: [SamplerAddressMode::ClampToEdge; 3],
            ..SamplerCreateInfo::simple_repeat_linear()
        },
    )
}

type EguiTexture = ImmutableImage;

#[derive(Debug, Error)]
pub enum CreateTextureError {
    #[error(transparent)]
    CreateImageFailed(#[from] ImageCreationError),
    #[error(transparent)]
    FlushFailed(#[from] FlushError),
    #[error(transparent)]
    IncorrectDefinition(#[from] DescriptorSetCreationError),
}
